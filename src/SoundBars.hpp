//
//  SoundBars.hpp
//  001
//
//  Created by nicola bertelloni on 06/04/2019.
//

#ifndef SoundBars_hpp
#define SoundBars_hpp

#include "ofMain.h"

class SoundBars : public ofBaseApp {
public:
  void draw(vector<float> melBands);
};

#endif /* SoundBars_hpp */
